# Gradle version must be the same as wrapper version in application code
FROM gradle:7.1.1-jdk11 AS gradle-build
WORKDIR /build
COPY . ./
RUN gradle clean build --info --stacktrace

FROM openjdk:11.0.11-jre-slim
COPY --from=gradle-build /build/build/libs/language-course-service-0.0.1-SNAPSHOT.jar /usr/app/language-course-service-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/language-course-service-0.0.1-SNAPSHOT.jar"]