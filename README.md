Program sklada sie z wielu klas oraz plikow konfiguracyjnych, dlatego prosta kompilacja poleceniem javac
jest niewygodna. Przygotowalem sie do cwiczenia i wiekszosc pracy wykonalem jeszcze w lipcu. Program korzysta z narzedzia Gradle i jest budowany jednym prostym
poleceniem. Nie potrzeba zadnych dodatkowych narzedzi poza Java 8.

Program jest kompletną aplikację internetową umozliwiajaca zarzadzanie katalogiem kursow
online.

Wszystkie poniższe polecenia należy uruchamiać w katalogu ze zrodlami

Budowa aplikacji Windows
```sh
gradlew build
```

Budowa aplikacji Linux
```sh
./gradlew build
```

Należy uruchomić bazę danych. Aplikacja używa domyślnych parametrów połączenia pakietu Xaamp,
więc można użyć bazy z pakietu. Należy tylko utworzyć samą bazę ```courses```.
Konfiguracja połączenia znajduje się w pliku ```src\main\resources\application.yaml```
```
jdbc:mysql://localhost:3306/courses
user: root
```

Uruchamianie aplikacji
```sh
java -jar build/libs/language-course-service-0.0.1-SNAPSHOT.jar
```

Domyślnie w bazie znajduje się użytkownik mający uprawnienia administracyjne.
Użytkownik może edytować kursy
```
user: admin
password: password
```

Aplikacja umożliwia:
* rejestrowaniie nowych użytkowników na podstronie Register
* logowanie na podstronie Login
* zarządzanie kursem przez administratora
* subskrybcję kursu przez zwyklego uzytkownika (studenta)
* potwierdzanie informacji o płatności

