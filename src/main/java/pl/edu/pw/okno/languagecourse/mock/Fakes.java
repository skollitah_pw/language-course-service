package pl.edu.pw.okno.languagecourse.mock;

import com.github.javafaker.Faker;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.edu.pw.okno.languagecourse.model.Course;
import pl.edu.pw.okno.languagecourse.model.User;
import pl.edu.pw.okno.languagecourse.model.Role;
import pl.edu.pw.okno.languagecourse.model.UserCourse;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fakes {

  private static final Random random = new Random();
  private static final Faker faker = new Faker(random);
  private static final List<String> courseNames =
      Arrays.asList(
          "Angielski",
          "Niemiecki",
          "Francuzki",
          "Polski",
          "Hiszpanski",
          "Rosyjski",
          "Wegierski",
          "Japonski");

  static List<User> mockUsers() {
    return Stream.generate(userSupplier())
        .limit(5 + new Random().nextInt(5))
        .collect(Collectors.toList());
  }

  private static Supplier<User> userSupplier() {
    return () -> {
      Role role = randomElement(Arrays.asList(Role.values()));
      String password = faker.ancient().god();
      return User.builder()
          .name(faker.gameOfThrones().character())
          .yearOfBirth(faker.number().numberBetween(1900, 2000))
          .role(role)
          .password(new BCryptPasswordEncoder().encode(password))
          .passwordPlain(password)
          .userCourses(new HashSet<>())
          .build();
    };
  }

  static List<Course> mockCourses() {
    return courseNames.stream()
        .map(val -> Course.builder().name(val).build())
        .collect(Collectors.toList());
  }

  static List<UserCourse> mockUserCourses(List<User> users, List<Course> courses) {
    return Stream.generate(userCourseSupplier(users, courses))
        .limit(5 + new Random().nextInt(10))
        .collect(Collectors.toList());
  }

  private static Supplier<UserCourse> userCourseSupplier(
          List<User> users, List<Course> courses) {
    return () -> {
      User user = randomElement(users);
      Course course = randomElement(courses);
      return UserCourse.builder()
          .user(user)
          .course(course)
          .paymentMade(faker.bool().bool())
          .build();
    };
  }

  private static <T> List<T> randomSubList(List<T> list) {
    return list.subList(0, randomIndex(list) + 1);
  }

  private static <T> T randomElement(List<T> list) {
    return list.get(randomIndex(list));
  }

  private static <T> int randomIndex(Collection<T> list) {
    return random.nextInt(list.size());
  }
}
