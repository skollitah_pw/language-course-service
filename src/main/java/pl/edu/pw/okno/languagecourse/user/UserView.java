package pl.edu.pw.okno.languagecourse.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserView {

  @NotEmpty private String name;

  @NotEmpty private String password;

  @NotEmpty private Integer yearOfBirth;
}
