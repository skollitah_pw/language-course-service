package pl.edu.pw.okno.languagecourse.maintance;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserCoursePayForm {

    @NotNull
    private Integer id;
}
