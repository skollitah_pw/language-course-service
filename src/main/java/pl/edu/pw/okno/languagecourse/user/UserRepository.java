package pl.edu.pw.okno.languagecourse.user;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.pw.okno.languagecourse.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

  Optional<User> findByName(String name);

  boolean existsByName(String name);

  void deleteByName(String name);
}
