package pl.edu.pw.okno.languagecourse.maintance;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pw.okno.languagecourse.model.UserCourse;
import pl.edu.pw.okno.languagecourse.user.UserCourseRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Controller
@RequestMapping
public class MaintenanceController {

  private final UserCourseRepository userCourseRepository;

  @GetMapping("/maintenance")
  public String getUserCourses(Model model) {

    List<UserCourse> userCourses = userCourseRepository.findAll();
    List<UserCourseView> userCourseViews =
        userCourses.stream().map(UserCourseView::from).collect(Collectors.toList());

    model.addAttribute("userCourses", userCourseViews);

    return "maintenance";
  }

  @PostMapping("/maintenance")
  @Transactional
  public String removeUserFromCourse(
      @ModelAttribute("userCourseDeleteForm") @Valid UserCourseDeleteForm userCourseDeleteForm) {

    userCourseRepository.customerDeleteById(userCourseDeleteForm.getId());

    return "redirect:maintenance";
  }

  @PostMapping("/payment")
  @Transactional
  public String payForCourse(
      @ModelAttribute("userCourseDeleteForm") @Valid UserCoursePayForm userCoursePayForm) {

    userCourseRepository
        .findById(userCoursePayForm.getId())
        .ifPresentOrElse(
            userCourse -> {
              userCourse.setPaymentMade(true);
              userCourseRepository.save(userCourse);
            },
            () -> {
              throw new IllegalArgumentException(
                  String.format("User Course not found by id - [%s]", userCoursePayForm.getId()));
            });

    return "redirect:maintenance";
  }
}
