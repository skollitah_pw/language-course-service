package pl.edu.pw.okno.languagecourse.model;

public enum Role {
  STUDENT,
  ADMIN
}
