package pl.edu.pw.okno.languagecourse.course;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CourseAddForm {

  @NotBlank private String name;
}
