package pl.edu.pw.okno.languagecourse.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pw.okno.languagecourse.model.User;
import pl.edu.pw.okno.languagecourse.model.Role;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
@RequiredArgsConstructor
public class UserController {

  private final UserRepository userRepository;

  @GetMapping
  public String home(Model model) {
    UserRegisterForm userRegisterForm = new UserRegisterForm();
    model.addAttribute("userRegisterForm", userRegisterForm);
    return "register";
  }

  @PostMapping
  public String registerUserAccount(
      @ModelAttribute("userRegisterForm") @Valid UserRegisterForm userRegisterForm, Errors errors) {

    if (userRepository.existsByName(userRegisterForm.getName())) {
      return "redirect:register";
    }

    User user =
        User.builder()
            .name(userRegisterForm.getName())
            .yearOfBirth(userRegisterForm.getYearOfBirth())
            .passwordPlain(userRegisterForm.getPassword())
            .password(new BCryptPasswordEncoder().encode(userRegisterForm.getPassword()))
            .role(Role.STUDENT)
            .build();

    userRepository.save(user);

    return "redirect:login";
  }
}
