package pl.edu.pw.okno.languagecourse.course;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pw.okno.languagecourse.model.Course;
import pl.edu.pw.okno.languagecourse.user.UserRepository;
import pl.edu.pw.okno.languagecourse.user.UserView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Controller
@RequestMapping({"/", "/courses"})
public class CourseController {

  private final CourseRepository courseRepository;
  private final UserRepository userRepository;

  @GetMapping
  public String getCourses(Model model) {

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    UserView userView =
        userRepository
            .findByName(authentication.getName())
            .map(
                user ->
                    UserView.builder()
                        .name(user.getName())
                        .yearOfBirth(user.getYearOfBirth())
                        .build())
            .orElse(new UserView());

    List<CourseView> courses =
        courseRepository.findAll().stream()
            .map(course -> CourseView.from(course, userView))
            .collect(Collectors.toList());

    model.addAttribute("user", userView);
    model.addAttribute("courses", courses);

    log.info(authentication.getName());

    return "courses";
  }

  @PostMapping
  public String addCourse(@ModelAttribute("courseAddForm") @Valid CourseAddForm courseAddForm) {

    Course course = Course.builder().name(courseAddForm.getName()).build();
    courseRepository.save(course);

    return "redirect:courses";
  }
}
