package pl.edu.pw.okno.languagecourse.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import pl.edu.pw.okno.languagecourse.model.UserCourse;

public interface UserCourseRepository extends JpaRepository<UserCourse, Integer> {

    @Modifying
    @Query("delete from UserCourse uc where uc.id = ?1")
    void customerDeleteById(Integer id);
}
