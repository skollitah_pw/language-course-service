package pl.edu.pw.okno.languagecourse.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {

  private final JpaUserDetailsManager userDetailsManager;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers("/maintenance/**")
        .hasRole("ADMIN")
        .antMatchers("/", "/home", "/register/**", "/courses/**", "/webjars/**")
        .permitAll()
        .antMatchers("/anonymous*")
        .anonymous()
        .anyRequest()
        .authenticated()
        .and()
        .formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
        .logout()
        .permitAll();
  }

  @Override
  public UserDetailsService userDetailsService() {
    return userDetailsManager;
  }
}
