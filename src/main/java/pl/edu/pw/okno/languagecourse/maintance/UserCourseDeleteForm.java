package pl.edu.pw.okno.languagecourse.maintance;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserCourseDeleteForm {

  @NotNull
  private Integer id;
}
