package pl.edu.pw.okno.languagecourse.utils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Pair<T, U> {
  private final T first;
  private final U second;

  public static <T, U> Pair<T, U> of(T first, U second) {
    return new Pair<>(first, second);
  }
}
