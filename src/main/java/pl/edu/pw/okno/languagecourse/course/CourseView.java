package pl.edu.pw.okno.languagecourse.course;

import lombok.Builder;
import lombok.Data;
import pl.edu.pw.okno.languagecourse.model.Course;
import pl.edu.pw.okno.languagecourse.user.UserView;

@Data
@Builder
public class CourseView {

  private Integer id;
  private String name;
  private boolean paid;
  private boolean registeredForCurrentUser;

  static CourseView from(Course course) {
    return CourseView.builder()
        .id(course.getId())
        .name(course.getName())
        .paid(false)
        .registeredForCurrentUser(false)
        .build();
  }

  static CourseView from(Course course, UserView currentUser) {

    CourseViewBuilder courseBuilder = CourseView.builder().id(course.getId()).name(course.getName());

    course.getUserCourses().stream()
        .filter(userCourse -> userCourse.getUser().getName().equals(currentUser.getName()))
        .findFirst()
        .ifPresent(
            userCourse ->
                courseBuilder.registeredForCurrentUser(true).paid(userCourse.isPaymentMade()));

    return courseBuilder.build();
  }
}
