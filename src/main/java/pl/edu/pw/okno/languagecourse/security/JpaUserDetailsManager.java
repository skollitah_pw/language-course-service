package pl.edu.pw.okno.languagecourse.security;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.languagecourse.model.User;
import pl.edu.pw.okno.languagecourse.model.Role;
import pl.edu.pw.okno.languagecourse.user.UserAlreadyExistException;
import pl.edu.pw.okno.languagecourse.user.UserRepository;

@RequiredArgsConstructor
@Component
public class JpaUserDetailsManager implements UserDetailsManager {

  private final UserRepository userRepository;

  @Override
  public void createUser(UserDetails userDetails) {

    userRepository
        .findByName(userDetails.getUsername())
        .ifPresent(
            user -> {
              throw new UserAlreadyExistException("User already exists");
            });

    String password =
        Hashing.sha256().hashString(userDetails.getPassword(), Charsets.UTF_8).toString();

    Role role = null;

    if (userDetails.getAuthorities().size() == 1) {
      role = Role.valueOf(userDetails.getAuthorities().iterator().next().getAuthority());
    }

    User user =
        User.builder()
            .name(userDetails.getUsername())
            .role(role)
            .password(password)
            .passwordPlain(userDetails.getPassword())
            .build();

    userRepository.save(user);
  }

  @Override
  public void updateUser(UserDetails userDetails) {
    User user =
        userRepository
            .findByName(userDetails.getUsername())
            .orElseThrow(() -> new UsernameNotFoundException("User not exists"));

    String password = new BCryptPasswordEncoder().encode(userDetails.getPassword());

    Role role = null;

    if (userDetails.getAuthorities().size() == 1) {
      role = Role.valueOf(userDetails.getAuthorities().iterator().next().getAuthority());
    }

    user.setRole(role);
    user.setPassword(password);
    user.setPasswordPlain(userDetails.getPassword());

    userRepository.save(user);
  }

  @Override
  public void deleteUser(String username) {
    userRepository.deleteByName(username);
  }

  @Override
  public void changePassword(String oldPassword, String newPassword) {}

  @Override
  public boolean userExists(String username) {
    return userRepository.existsByName(username);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user =
        userRepository
            .findByName(username)
            .orElseThrow(() -> new UsernameNotFoundException("User not exists"));

    return org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder()
        .username(user.getName())
        .password(user.getPasswordPlain())
        .roles(user.getRole().name())
        .build();
  }
}
