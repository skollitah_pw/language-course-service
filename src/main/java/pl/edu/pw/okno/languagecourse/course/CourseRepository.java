package pl.edu.pw.okno.languagecourse.course;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.pw.okno.languagecourse.model.Course;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Integer> {

  Optional<Course> findByName(String name);
}
