package pl.edu.pw.okno.languagecourse.maintance;

import lombok.Builder;
import lombok.Data;
import pl.edu.pw.okno.languagecourse.model.UserCourse;

@Data
@Builder
public class UserCourseView {

  private Integer id;
  private String userName;
  private String courseName;
  private boolean paid;

  public static UserCourseView from(UserCourse userCourse) {
    return UserCourseView.builder()
        .id(userCourse.getId())
        .userName(userCourse.getUser().getName())
        .courseName(userCourse.getCourse().getName())
        .paid(userCourse.isPaymentMade())
        .build();
  }
}
