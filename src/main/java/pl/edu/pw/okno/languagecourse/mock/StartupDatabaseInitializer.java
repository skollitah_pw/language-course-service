package pl.edu.pw.okno.languagecourse.mock;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.edu.pw.okno.languagecourse.course.CourseRepository;
import pl.edu.pw.okno.languagecourse.model.Course;
import pl.edu.pw.okno.languagecourse.model.User;
import pl.edu.pw.okno.languagecourse.model.Role;
import pl.edu.pw.okno.languagecourse.user.UserCourseRepository;
import pl.edu.pw.okno.languagecourse.user.UserRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class StartupDatabaseInitializer implements ApplicationListener<ContextRefreshedEvent> {

  private final UserRepository userRepository;
  private final CourseRepository courseRepository;
  private final UserCourseRepository userCourseRepository;

  @Override
  @Transactional
  public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    log.info("Application started");

    List<User> users =
        Fakes.mockUsers().stream().map(userRepository::save).collect(Collectors.toList());

    User admin =
        User.builder()
            .name("admin")
            .yearOfBirth(1983)
            .passwordPlain("password")
            .password(new BCryptPasswordEncoder().encode("password"))
            .role(Role.ADMIN)
            .build();

    userRepository.save(admin);

    List<Course> courses =
        Fakes.mockCourses().stream().map(courseRepository::save).collect(Collectors.toList());

    Fakes.mockUserCourses(users, courses).forEach(userCourseRepository::save);
  }
}
