package pl.edu.pw.okno.languagecourse.userCourses;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CourseSubscribeForm {

  @NotNull private Integer id;
}
