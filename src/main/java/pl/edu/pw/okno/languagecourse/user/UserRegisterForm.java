package pl.edu.pw.okno.languagecourse.user;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class UserRegisterForm {

  @NotEmpty private String name;

  @NotEmpty private String password;

  @NotNull @Positive private Integer yearOfBirth;
}
