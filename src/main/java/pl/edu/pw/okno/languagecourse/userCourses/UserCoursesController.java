package pl.edu.pw.okno.languagecourse.userCourses;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pw.okno.languagecourse.course.CourseRepository;
import pl.edu.pw.okno.languagecourse.model.Course;
import pl.edu.pw.okno.languagecourse.model.User;
import pl.edu.pw.okno.languagecourse.model.UserCourse;
import pl.edu.pw.okno.languagecourse.user.UserCourseRepository;
import pl.edu.pw.okno.languagecourse.user.UserRepository;

import javax.validation.Valid;
import java.util.Optional;

@RequiredArgsConstructor
@Controller
@RequestMapping({"/userCourses"})
public class UserCoursesController {

  private final UserCourseRepository userCourseRepository;
  private final UserRepository userRepository;
  private final CourseRepository courseRepository;

  @PostMapping
  public String addCourse(
      @ModelAttribute("courseSubscribeForm") @Valid CourseSubscribeForm courseSubscribeForm) {

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Optional<User> user = userRepository.findByName(authentication.getName());

    if (user.isEmpty()) {
      return "redirect:courses";
    }

    Optional<Course> course = courseRepository.findById(courseSubscribeForm.getId());

    if (course.isEmpty()) {
      return "redirect:courses";
    }

    UserCourse userCourse =
        UserCourse.builder().paymentMade(false).user(user.get()).course(course.get()).build();

    userCourseRepository.save(userCourse);

    return "redirect:courses";
  }
}
